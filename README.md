[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

# Dependencies
- [CMake](https://cmake.org/download/)
- [Qt 5](http://www.qt.io/download-open-source/)
- [Wiring Pi](http://wiringpi.com/download-and-install/)

The program has been tested on Raspbian and Ubuntu Mate for Raspberry Pi.

Install CMake:
```bash
sudo apt install cmake
```

Install Qt 5:
```bash
sudo apt install qttools5-dev qt5-default
```

Wiring Pi is installed by default, you can install it with:
```bash
sudo apt install wiringPi
```

# How to build
Clone the project:
```bash
mkdir -p raspberry_stepper_demo
cd raspberry_stepper_demo
git clone https://gitlab.com/InstitutMaupertuis/raspberry_stepper_demo.git src
```

Build using `cmake`:
```bash
mkdir build_release
cd build_release
cmake ../src -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="../install"
make -j2
```

Install the project with:
```bash
make install
```

# Pin connections
- Connect the `STEP` pin of the DVR8825 on the [BCM 23 pin](http://pinout.xyz/pinout/pin16_gpio23)
- Connect the `DIR` pin of the DVR8825 on the [BCM 24 pin](http://pinout.xyz/pinout/pin18_gpio24)

The pins are set in the main window constructor: [mainwindow.cpp](./mainwindow.cpp#L9).

# Launching
The install target creates a desktop launcher:

![Desktop launcher](documentation/screen_0.png)

# Usage

![Screen 1](documentation/screen_1.png)

![Screen 2](documentation/screen_2.png)

If the motor is not moving:
- Make sure the motor is running ok by triggering the GPIOs manually.
- Make sure the program is run as root (the launcher does that by default)

