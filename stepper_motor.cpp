#include <stepper_motor.hpp>

//#define SIMULATION
#ifndef SIMULATION
#include <wiringPi.h>
#endif

//#define DEBUG_TIME
#ifdef DEBUG_TIME
#include <numeric>
#endif

StepperMotor::StepperMotor(const unsigned step_pin,
                           const unsigned dir_pin) :
        step_pin_(step_pin),
        dir_pin_(dir_pin),
        current_angle_(0),
        current_velocity_(0),
        is_moving_(false),
        step_angle_(M_PI / 100.0), // 1.8 degrees
        micro_stepping_(1.0),
        gearing_ratio_(1.0),
        steps_(0),
        infinite_rotation_(0),
        clockwise_(true),
        step_delay_(100000000),
        stop_stepping_(false),
        is_locked_(false)
{
#ifndef SIMULATION
  wiringPiSetup();
  std::cout << "Setting WiringPi pin " << std::to_string(step_pin) << " step pin to OUTPUT" << std::endl;
  pinMode(dir_pin_, OUTPUT);
  std::cout << "Setting WiringPi pin " << std::to_string(dir_pin) << " dir pin to OUTPUT" << std::endl;
  pinMode(step_pin_, OUTPUT);
#endif
}

StepperMotor::~StepperMotor()
{
  stop(true);
}

void StepperMotor::configure(const double step_angle,
                             const double micro_stepping,
                             const double gearing_ratio)
{
  if (is_locked_)
  {
    std::cout << " StepperMotor is locked. Abort configure()" << std::endl;
    return;
  }
  is_locked_ = true;

  if ((step_angle <= 0) | (micro_stepping <= 0) | (gearing_ratio <= 0))
    std::cout << name_ << " Step angle, micro stepping or gearing ratio < 0. Aborting configure()" << std::endl;

  step_angle_ = step_angle;
  micro_stepping_ = micro_stepping;
  gearing_ratio_ = gearing_ratio;

  is_locked_ = false;
}

void StepperMotor::resetAngle()
{
  if (is_locked_)
  {
    std::cout << " StepperMotor is locked. Abort configure()" << std::endl;
    return;
  }

  is_locked_ = true;
  current_angle_ = 0;
  is_locked_ = false;
}

bool StepperMotor::isMoving()
{
  return is_moving_;
}

double StepperMotor::getAngle()
{
  return (current_angle_);
}

double StepperMotor::getVelocity()
{
  return (current_velocity_);
}

unsigned StepperMotor::getPercentageComplete()
{
  return (percentage_complete_);
}

void StepperMotor::stop(const bool blocking)
{
  if (!is_moving_ && !is_locked_)
    return;

  stop_stepping_ = true;

  if (blocking)
  {
    // Wait for thread to finish last iteration and exit
    if (worker_.joinable())
      worker_.join();
    else
    {
      // FIXME Why isn't the thread joinable?
      while (is_moving_ && is_locked_)
      {
      }
    }
  }
}

void StepperMotor::moveSteps(const unsigned steps,
                             const bool clockwise,
                             const std::chrono::nanoseconds &step_delay,
                             const std::chrono::nanoseconds &start_delay,
                             const bool interupt)
{
  if (interupt)
    stop(true);
  else if (is_locked_)
  {
    std::cout << name_ << " StepperMotor is locked. Abort moveSteps()" << std::endl;
    return;
  }
  is_locked_ = true;

  move(steps, clockwise, step_delay, start_delay);
}

void StepperMotor::moveToAngle(double target_angle,
                               const std::chrono::nanoseconds &time,
                               const bool use_step_delay,
                               const bool force_clockwise,
                               const std::chrono::nanoseconds &start_delay,
                               const bool interupt)
{
  if (interupt)
    stop(true);
  else if (is_locked_)
  {
    std::cout << name_ << " StepperMotor is locked. Abort moveToAngle()" << std::endl;
    return;
  }
  is_locked_ = true;

  if (time == std::chrono::nanoseconds(0))
  {
    std::cout << name_ << " Time = 0. Aborting moveToAngle()" << std::endl;
    is_locked_ = false;
    return;
  }

  constrainAngle(target_angle);

  // How much does the motor need to travel? (smallest distance!)
  // http://stackoverflow.com/questions/1878907/the-smallest-difference-between-2-angles
  double angle_to_travel(target_angle - current_angle_);
  angle_to_travel += (angle_to_travel > M_PI) ? -2 * M_PI : ((angle_to_travel < -M_PI) ? 2 * M_PI : 0);

  // If angel to travel is smaller than the smallest angle the motor can move, ignore movement!
  if (fabs(angle_to_travel) < step_angle_ * micro_stepping_ * gearing_ratio_)
  {
    std::cout << name_ << " 0 step necessary! Ignoring moveToAngle()" << std::endl;
    percentage_complete_ = 100;
    is_locked_ = false;
    return;
  }

  bool clockwise(true);
  if (angle_to_travel >= 0) // >=0 means anti clock wise (angle is in radians)
    clockwise = false;

  if (force_clockwise && clockwise == false)
  {
    angle_to_travel = 2 * M_PI - angle_to_travel;
    clockwise = true;
  }

  // Compute best number of steps to reach angle
  double angle_offset(step_angle_ * micro_stepping_ * gearing_ratio_);
  unsigned steps = std::round((long double)fabs(angle_to_travel) / angle_offset);

  if (use_step_delay)
  {
    move(steps, clockwise, time, start_delay);
    return;
  }

  // Compute speed (= step_delay) to reach the position in time
  std::chrono::nanoseconds step_delay;
  step_delay = time / steps;
  move(steps, clockwise, step_delay, start_delay);
}

void StepperMotor::moveToVelocity(const std::chrono::nanoseconds &revolution_duration,
                                  const bool clockwise,
                                  const std::chrono::nanoseconds &start_delay,
                                  const bool interupt)
{
  if (interupt)
    stop(true);
  else if (is_locked_)
  {
    std::cout << name_ << " StepperMotor is locked. Abort moveToVelocity()" << std::endl;
    return;
  }
  is_locked_ = true;

  if (revolution_duration == std::chrono::nanoseconds::zero())
  {
    std::cout << name_ << " Revolution duration = 0. Aborting moveToVelocity()" << std::endl;
    is_locked_ = false;
    return;
  }

  // Compute speed (= step_delay) to make a full revolution (= 360° = 2*M_PI) in time
  unsigned steps_per_revolution(2 * M_PI / (step_angle_ * micro_stepping_ * gearing_ratio_));
  if (steps_per_revolution == 0) // Should never happen because configure() checks for wrong values!
  {
    std::cout << name_ << " Steps per revolution = 0. Aborting moveToVelocity()" << std::endl;
    is_locked_ = false;
    return;
  }

  std::chrono::nanoseconds step_delay(revolution_duration / steps_per_revolution);
  move(1, clockwise, step_delay, start_delay, true);
}

// Private

void StepperMotor::constrainAngle(double &theta)
{
  theta = fmod(theta + M_PI, 2 * M_PI);
  if (theta < 0)
    theta += 2 * M_PI;
  theta -= M_PI;
}

void StepperMotor::steppingCallback()
{
  if (start_delay_ != std::chrono::nanoseconds::zero())
    std::this_thread::sleep_for(start_delay_);

  uint64_t goal_steps(steps_);
  is_moving_ = true;
  current_velocity_ = (clockwise_ ? -1 : 1); // FIXME Compute speed in rad/sec!

  // Compute real step angle
  double angle_offset(step_angle_ * micro_stepping_ * gearing_ratio_);
  if (clockwise_)
  {
    angle_offset = -angle_offset;
#ifndef SIMULATION
    digitalWrite(dir_pin_, HIGH);
#endif
  }
#ifndef SIMULATION
  else
    digitalWrite(dir_pin_, LOW);
#endif

  if (infinite_rotation_)
  {
    percentage_complete_ = 100;
    steps_ = 1;
  }

#ifdef DEBUG_TIME
  std::vector<unsigned> duration;
#endif

  while (!stop_stepping_ && steps_ != 0)
  {
#ifdef DEBUG_TIME
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
#endif

    double angle = current_angle_ + angle_offset;
    constrainAngle(angle);
    current_angle_ = angle;

    if (!infinite_rotation_)
    {
      --steps_;
      percentage_complete_ = 100 - (100 * (double)steps_ / goal_steps);
    }

#ifndef SIMULATION
    // Real step
    digitalWrite(step_pin_, HIGH);
    delayMicroseconds(1);
    digitalWrite(step_pin_, LOW);
#endif

#ifdef DEBUG_TIME
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    duration.push_back(std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count());
#endif

    // Sleep between each step
    std::this_thread::sleep_for(step_delay_);
  }

#ifdef DEBUG_TIME
  double mean (1.0 * std::accumulate(duration.begin(), duration.end(), 0LL) / duration.size());
  std::cout << "Mean value over " << duration.size() << " samples = " << mean << " nsec" << std::endl;
#endif

  double angle(current_angle_);
  constrainAngle(angle);
  current_angle_ = angle;

  percentage_complete_ = 100;

  stop_stepping_ = false;
  is_moving_ = false;
  current_velocity_ = 0;
  is_locked_ = false;
}

void StepperMotor::move(const uint64_t steps,
                        const bool clockwise,
                        const std::chrono::nanoseconds &step_delay,
                        const std::chrono::nanoseconds &start_delay,
                        const bool infinite_rotation)
{
  is_locked_ = true;

  if (step_delay == std::chrono::nanoseconds::zero() || steps == 0)
  {
    if (steps != 0)
      std::cout << name_ << " Step delay cannot be null! Aborting move()" << std::endl;
    else
    {
      std::cout << name_ << " 0 step asked! Ignoring move()" << std::endl;
      percentage_complete_ = 100;
      return;
    }

    is_locked_ = false;
    return;
  }

  percentage_complete_ = 0;

  steps_ = steps;
  clockwise_ = clockwise;
  step_delay_ = step_delay;
  infinite_rotation_ = infinite_rotation;
  start_delay_ = start_delay;

  // Start thread and detach it
  worker_ = std::thread(&StepperMotor::steppingCallback, this);
  worker_.detach();
}
