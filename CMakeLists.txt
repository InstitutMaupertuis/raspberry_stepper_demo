cmake_minimum_required(VERSION 3.2)
project(raspberry_stepper_demo)
add_compile_options(-Wall -Wextra)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(
  CMAKE_MODULE_PATH
  ${CMAKE_MODULE_PATH}
  "${CMAKE_SOURCE_DIR}/cmake/Modules/"
)
find_package(WiringPi REQUIRED)
include_directories(${WIRINGPI_INCLUDE_DIRS})

find_package(Qt5Widgets REQUIRED)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

QT5_WRAP_UI(
  UIS
  mainwindow.ui
)

add_library(
  stepper_motor
  stepper_motor.cpp
)

add_executable(
  ${PROJECT_NAME} ${UIS}
  main.cpp
  mainwindow.cpp
)

target_link_libraries(
  ${PROJECT_NAME}
  stepper_motor
  pthread
  Qt5::Widgets
  ${WIRINGPI_LIBRARIES}
)

# Install
# FIXME Only works with local installs!
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_INSTALL_PREFIX}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_INSTALL_PREFIX}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_INSTALL_PREFIX}/bin)

install(TARGETS
  ${PROJECT_NAME}
  stepper_motor
  ARCHIVE DESTINATION ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}
  LIBRARY DESTINATION ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}
  RUNTIME DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
)

# Create directory for the Ubuntu desktop file/icon
# This only works on Ubuntu and for the current user
set(HOME_DIR $ENV{HOME})
set(UBUNTU_DESKTOP_APP_DIR "${HOME_DIR}/.local/share/applications")
install(DIRECTORY DESTINATION ${UBUNTU_DESKTOP_APP_DIR})
# Configure / install desktop launcher
configure_file(desktop/${PROJECT_NAME}.desktop.in ${UBUNTU_DESKTOP_APP_DIR}/${PROJECT_NAME}.desktop)
# Install icon
install(FILES
  desktop/${PROJECT_NAME}.png
  DESTINATION ${UBUNTU_DESKTOP_APP_DIR}
)

