#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <stepper_motor.hpp>

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

private slots:
  void updateAngle();
  void moveToAngle();
  void on_pushButton_stop_clicked();
  void on_pushButton_start_clicked();
  void on_pushButton_reset_clicked();

private:
  Ui::MainWindow *ui_;
  std::shared_ptr<StepperMotor> motor_;
};

#endif // MAINWINDOW_H
